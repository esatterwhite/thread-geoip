var nconf = require('nconf')

module.exports = nconf
					.argv()
					.env({separator:'__'})
					.defaults( require('./defaults' ) )
