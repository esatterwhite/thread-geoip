/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * Safely merges clones an object
 * @module threadmeup/clone
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires threadmeup/typeOf
 */

var typeOf = require( './typeOf' )
  , is     = require('./is')
  ;

/**
 * Copies properties from 0 or more object to the target object
 * @private
 * @function
 * @name mixin
 * @param {Object} target
 * @param {..Object} object Object to mixin to the target
 * @return {TYPE} DESCRIPTION
 **/
function mixin(target /*[,objects] */){
	let i = 0                // Current position
	  , n = arguments.length // number of arguments
	  , obj                  // Object we are mixing
	  ;

	while(++i < n){
		obj = arguments[i];
		if (obj != null) {
			for(let key in obj ){
				target[key] = obj[key];
			}
		}
	}
	return target;
}

/**
 * clones properties of an object. 
 * @private
 * @function
 * @name cloneObject
 * @param {Object} source The object to clone
 * @return {Object} obj A new object with the properties of the source
 **/
function cloneObject(source) {
	if (is.simpleObject( source )) {
		return mixin( {}, source );
	} else {
		return source;
	}
}

/**
 * Clones a Regular expression object
 * @private
 * @function
 * @name cloneRegExp
 * @param {RegExp} expr The expression to clone 
 * @return {RegExp} A new regular expression
 **/
function cloneRegExp(expr) {
	let flags = ''; // holder for expression flags;

	flags += expr.multiline ? 'm' : '';
	flags += expr.global ? 'g' : '';
	flags += expr.ignoreCase ? 'i' : '';
	return new RegExp(expr.source, flags);
}

/**
 * Clones a Date Object. Not great. Loses TZ information
 * @private
 * @function 
 * @name cloneDate
 * @param {Date} date The original date to clone
 * @return {Date} A new Date matching the original
 **/
function cloneDate(date) {
	return new Date(+date);
}

/**
 * Shallow clones an array
 * @private
 * @function
 * @name cloneArray
 * @param {Array} arr The array to clone
 * @return {Array} A new copy of the original
 **/
function cloneArray(arr) {
	return arr.slice();
}

/**
 * @private
 * @function
 * @alias module:threadmeup/clone
 * @name clone
 * @param {Object} val The object to clone
 * @return {Object} A clone of the original object
 **/
function clone(val){
	switch (typeOf(val)) {
		case 'object':
			return cloneObject(val);
		case 'array':
			return cloneArray(val);
		case 'regExp':
			return cloneRegExp(val);
		case 'date':
			return cloneDate(val);
		default:
			return val;
	}
}

module.exports = clone;