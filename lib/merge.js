/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * Safely merges multiple object together
 * @module threadmeup/merge
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires threadmeup/is
 * @requires threadmeup/typeOf
 */

var is = require('./is')
  , typeOf = require('./typeOf')
  , clone = require('./clone')
  ;


/**
 * All knowning hasOwnProperty
 * @private
 * @function
 * @name hasOwn
 * @param {Object} obj The Object to inspect
 * @param {String} prop The property name to check
 * @return {Boolean}
 **/
function hasOwn( obj, prop ){
	return Object.prototype.hasOwnProperty.call( obj, prop );
}

/**
 * merges all passed in object in to the first object passed in ( modifies the first arguemnts)
 * @function
 * @alias module:threadmeup/merge
 * @param {...Object} objects the obects to merge
 * @return {Object} A single merged object
 **/
function merge( /*[ object, [object ...] ] */){
	let i = 1
	  , key
	  , val
	  , obj
	  , target
	  ;

	// lets be non-desctructive!
	target = clone( arguments[0] );

	while (obj = arguments[i++]) {
		for (key in obj) {
			if ( ! hasOwn(obj, key) ) {
				continue;
			}

			val = obj[key];

			if ( is.object(val) && is.object(target[key]) ){
				// inception, deep merge objects
				target[key] = merge(target[key], val);
			} else {
				// make sure arrays, regexp, date, objects are cloned
				target[key] = clone(val);
			}

		}
	}

	return target;
}

module.exports = merge;