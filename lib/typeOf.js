/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true */
'use strict';
/**
 * A better typeof function, returns the type of object
 * @module threadmeup/typeOf.js
 * @author Eric Satterwhite
 * @since 1.0.0
 */

const jsexp    = /^\[object (.*)\]$/;

/**
 * @private
 * @function
 * @alias module:threadmeup/typeOf
 * @param {Object} obj The object of whose type to determin
 * @return {String} type
 **/
function typeOf( obj ){
	return ( jsexp.exec( Object.prototype.toString.call( obj || {} ) )[1]).toLowerCase()
};

module.exports = typeOf;