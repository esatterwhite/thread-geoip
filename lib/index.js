/*jshint laxcomma: true, smarttabs: true, esnext: true */
'use strict';
/**
 * Koa middleware for geolocating a request origin
 * @module threadmeup
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requirest geoip-lite
 * @requires co-request
 * @requires debug
 * @requires threadmeup/lib/geo
 */
var debug   = require('debug')('threadmeup:geoip')
  , Geo     = require( './geo' )
  ;


module.exports = function( opt ){
	let geo = new Geo( opt ); // geo location instance for the middleware instance
	
	return function *GeoIp( next ){
		debug( this.path );
		if( !geo.exempt( this.path ) ){
			debug('ip lookup')
			var ip = yield geo.ip( this.req );
			this.state.ip = ip;
			debug('location lookup for %s', ip);
			this.state.location = ip ? geo.locate( "" + ip  ) : null
			debug('location complete', this.state.location);
		} else {
			debug('%s exempt from geoip', this.path );
		}
		yield next;
	}
}
