/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * shortcut helpers for typeOf === 'foos'
 * @module is.js
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires threadmeup/typeof
 */
var typeOf = require('./typeOf');

/**
 * Checks if something is explicelty an object
 * @param {Object} obj The object to check
 * @return {Boolean}
 **/
exports.object = function( obj ){
	return typeOf( obj ) === 'object';
};

/**
 * Checks to determine of an object is an object literal
 * @param {Object} value The object to check
 * @return {Boolean} DESCRIPTION
 **/
exports.simpleObject = function(value) {
	return (!!value && typeof value === 'object' && value.constructor === Object);
};
