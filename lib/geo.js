/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * Class used to derive an IP and geo location origination for incoming requests
 * @module threadmeup/geo
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires geoip-lite
 * @requires debug
 * @requires threadmeup/conf
 * @requires path-to-regexp
 */

var geoip  = require( 'geoip-lite' )              // ip geo location databases
  , debug  = require( 'debug' )('threadmeup:geo') // debug modeule
  , pexp   = require( 'path-to-regexp' )          // express|koa uri parser
  , net    = require( 'net' )                     // node net module
  , co     = require('co')                        // co module
  , loc     = require('geoip-lite')               // location databases
  , get    = require('co-request')                // used to get public IP if request comes from a non routable IP 
  , typeOf = require('./typeOf')                  // better type of module
  , merge  = require('./merge')                   // merge helper for options
  , clone  = require('./clone')                   // clone helper for options
  , is     = require('./is')                      // shortcut type helper
  , conf   = require( '../conf' )                 // configuration
  ;

let defaults   = conf.get( 'threadmeup:middleware' ); // default options for parser instances. 

const loopipv4 = '127.0.0.1'; // ipv4 loopback ip
const loopipv6 = '::1';       // ipv6 loopback ip

/**
 * @constructor
 * @alias module: threadmeup/geo
 * @param {Object} [options]
 * @param {String[]} [options.ipv4_private_c]
 * @param {String[]} [options.ipv4_private_a]
 * @param {String[]} [options.ipv4_private_b]
 * @param {String[]} [options.ipv4_private_broadcast]
 * @param {String[]} [options.ipv4_loopback]
 * @param {String[]} [options.ipv6_private_broadcast]
 * @param {String[]} [options.ipv6_private_multicast]
 * @param {String[]} [options.ipv6_private_loopback]
 * @param {String[]} [options.header_precedence]
 * @param {?String} [options.exempt=null] A url pattern that is exempt from geo location
 */
class Parser{
	constructor( opts ){
		this.options  = this.options || clone( defaults );
		this.setOptions( opts );
		this.non_routable = this.genNonRoutable();

		// convert exempt uri to an expression...
		this.options.exempt = typeOf( this.options.exempt ) == 'string' ? pexp( this.options.exempt ) : this.options.exempt;
	}


	/**
	 * DESCRIPTION
	 * @method module:threadmeup/geo#setOptions
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return {TYPE} DESCRIPTION
	 **/
	setOptions( /*[ object, [object ...] ] */ ){
		this.options = merge.apply( null, [ {}, defaults  ].concat( Array.prototype.slice.apply( arguments ) ) );
	}

	/**
	 * Generates T single list of configured non routable ips
	 * @method module:threadmeup/geo#genNonRoutable
	 * @return {String[]}
	 **/
	genNonRoutable(){
		let list = [];
		for( let key in this.options ){
			if( key.indexOf( 'ipv4_') >= 0 || key.indexOf( 'ipv6_' ) >= 0 ){
				list = list.concat( this.options[ key ] );
			}
		}

		return list;
	}

	/**
	 * Creates a generator which resolves the origination ip of a request
	 * @method module:threadmeup/geo#ip
	 * @param {Request} req A koa request object
	 * @return {Function} A generator function
	 **/
	ip( request ){

		let header_value = '' ;
		let precedence   = this.options.header_precedence;
		let len          = precedence.length;
		let pos          = 0;
		let headers      = request.headers || {};
		let result       = null;
		let ips          = [];

		while( pos < len ){
			header_value = headers[ precedence[ pos ] ];
			if( header_value ){
				break;
			}
			pos++;
		}
		
		// assume a list of ips
		ips = ( header_value || (request.socket || request.connection).remoteAddress || '' ).split(',');

		for( let x = 0; x<ips.length; x++){
		
			let ip = ips[ x ].toLowerCase().trim();

			if( net.isIP( ip ) ){
				if( this.isRoutable( ip )){
					return function *(){
						return yield Promise.resolve( ip );
					}
				}
			}
		};

		return function *(){
			var res = yield get('http://ipecho.net/plain');
			return yield Promise.resolve( res.body );
		};
	}


	/**
	 * Determines if an IP address is externally routable
	 * @method module:threadmeup/geo#isRoutable
	 * @param {String} ip An ipv4 or ipv6 address
	 * @return {Boolean}
	 **/
	isRoutable( ip ){
		return !this.isLoopback( ip ) && !this.isPrivate( ip );
	}

	/**
	 * Determines if an ip is considered a private address
	 * @method module:threadmeup/geo#isPrivate
	 * @param {String} ip A valid Ip Address
	 * @return {Boolean}
	 **/
	isPrivate( ip ){
		let ips = this.non_routable;
		for( let x=0, len=ips.length; x<len; x++){
			if( ip.indexOf( ips[ x ].toLowerCase() ) === 0){
				return true;
			}
		}

		return false;
	}

	/**
	 * Determines if an address is an internal loopback address
	 * @method module:threadmeup/geo#isLoopback
	 * @param {String} ip A valid Ip Address
	 * @return {Boolean}
	 **/
	isLoopback( ip ){
		return ip === loopipv4 || ip === loopipv6;
	}

	/**
	 * Determines of a request path is exempt for geo location
	 * @method module:threadmeup/geo#exempt
	 * @param {String} path the path being requested
	 * @return {Boolean}
	 **/
	exempt( pth ){
		return this.options.exempt ? !!this.options.exempt.exec( pth ) : false;
	}

	locate( ip ){
		return loc.lookup( ip )
	}
}

module.exports = Parser;
