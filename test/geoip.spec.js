'use strict';
var assert = require('assert')
  , should = require('should')
  , net = require('net')
  , Geo = require('../lib/geo')
  , co  = require('co')
  , request = require('co-request')
  , koarequest = require('./context')

describe('GEO Locator', function( ){
	var geo

	before(function( done ){
		geo = new Geo({exempt:"/api/:action/:optional?"});
		done();
	});


	describe('#loopback', function(){
		it("should recognize ipv4", function(){
			assert.ok( geo.isLoopback('127.0.0.1') )
			assert.ok( !geo.isLoopback('70.1.1.1') )
		});

		it('should recognize ipv6', function(){
			assert.ok( geo.isLoopback('::1') )
			assert.ok( !geo.isLoopback('fe80::cc3e:f88c:7cb8:32bd') )
		});
	});

	describe('#private', function(){
		it("should recognize ipv4", function(){
			assert.ok( geo.isPrivate('127.0.0.1') );
			assert.ok( !geo.isPrivate('70.1.1.1') );
		});

		it('should recognize ipv6', function(){
			assert.ok( geo.isPrivate('::1') );
			assert.ok( !geo.isPrivate('fe80::cc3e:f88c:7cb8:32bd') );
		});
	})

	describe("#ip", function(){
		it('should yield a valid IP', function( done ){
			
			this.timeout( 4000 )

			co(function *(){
				let req = koarequest();
				req.socket.remoteAddress = '127.0.0.1';
				let ip = yield geo.ip( req.request );
				return Promise.resolve( ip );
			}).then( function( ip ){
				var value = net.isIP( ip );
				assert.ok( ip );
				assert.ok( value == 4 || value == 6 );
				done();
			}).catch( function( err  ){
				console.log( err );
				done( err );
			})
		})
	})
	describe('~exemptions', function(){
		it('should honor exempt url patterns', function(){
			assert.ok( !geo.exempt('/api/'));
			assert.ok( !geo.exempt('/') );
			assert.ok( geo.exempt('/api/foo') );
			assert.ok( geo.exempt('/api/foo/bar') );
		})
	})
})