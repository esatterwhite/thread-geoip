// polyfill koa request context;
//
var Stream = require('stream');
var koa = require('koa')

exports = module.exports = function( req, res ){
	var socket = new Stream.Duplex();
	req = req || {headers:{}, socket: socket, connection: socket, __proto__: Stream.Readable.prototype }
	res = res || {headers:{}, socket: socket, connection: socket, __proto__: Stream.Writable.prototype }

	res.getHeader = function( key ){ return res._headers[ key.toLowerCase() ] };
	res.setHeader = function( key, val ){ res._headers[ key.toLowerCase() ] = val };
	res.removeHeader = function( key ){ delete res._headers[key.toLowerCase()]};

	return koa().createContext( req, res );
}

exports.request = function( req, res ){
	return exports( req, res ).request;
}
exports.response =function( req, res ){
	return exports( req, res ).response;
}
