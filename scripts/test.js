/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Simple test runner. Forks a mocha process. Done!
 * @module test.js
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires util
 * @requires child_process
 * @requires debug
 */

var util          = require( 'util' )
  , child_process = require( 'child_process' )
  , debug         = require( 'debug' )("thremeup:test")
  , test          = ( process.env.NODE_ENV == 'test' )
  , mocha
  ;


  mocha = child_process.spawn("mocha", [
  	"--growl"
  	, "--recursive"
	// , '--debug-brk'
	, util.format("--reporter=%s", test ? 'xunit':'spec')
  	, 'test/*.spec.js'
  ])

  mocha.on('exit', function( code, sig){
  	process.exit( code )
  })

  mocha.stdout.pipe( process.stdout );
  mocha.stderr.pipe( process.stderr );
