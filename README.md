Process
=======
1. Keep as much functionality as possible out of the middleware itself.
    1. Should be usable / re-usabel & testable outside the context of Koa.
    2. defaults should be configurable & overridable - non-destructive
    3. If I wanted to use this else where in a project or in a different project ( without koa ), i should be able to

2. Have a way to locate local / private ips
    1. for development & testing, you will more than likely be working over the loop back
       or a private network, which can't be geolocated. so as a fall back we need something else to
       give us a best possible guess

3. keep syntax of exemptions simple and familiar.
    1. allow users to give a route definition or a Regex to denote which routes should be excluded

4. Look really hard for an IP address
    1. between Node, proxies and server, the ip address can hidden in a number of places.
       Check them before we ping a service.

5. configurable
    - Each instance is configurable, but the default set up is as well via nconf which is pulling from multiple places this makes configuring easier for automated deployments, and doesn't force end users to hard code thing in their apps to alter behaviors.
    - and because we are using nconf to specify the defaults, route exemptions can be specified as ENV vars, or command line flags, and out of the application. reduces the need to re-deloy to make changes / tweeks.

6. Keep it modular
    1. tied to keep things logically separate where appropriate i.e a UNIX mindset.

Needs Work
==========

1. The actual ip look up function requires something that looks like a request object, which violates a lot of the re/usablity
    1. the logic that picks out an IP could be moved to the middle ware, and the address could be passed to the geo instance

2. caching / result storage
    1. we could cache the results of the ip & location 
        * assuming a browser, could set the ip in httponly cookies so we can avoid a slow lookups
        * A swappable storage backend ( memory, redis, etc ); and allow an option to denote the storage type `geo( { exempt:'/foo', storage:'redis" })` 

3. the GEO databases are not configurable. They should be. They are also big data files...

4. No layer between the data returned from the geo look up and the output.
    1. that means that any change in the library or data sets would be a breaking change for the middle ware. A little reshaping function would help eliminate that.

5. There is no Error handling / zero state
    1. No real good checking for valid inputs or catching for things that might fail
    2. if the external look up fallback fails, this is pretty useless
    3. No sensible retry on external look up.
   

Running
=======

```bash
npm install
npm start
curl http://localhost:3000
curl http://localhost:3000/api/exempt
```